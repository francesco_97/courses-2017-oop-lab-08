package it.unibo.oop.lab.mvcio;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;


/**
 * 
 * 
 *
 * @param <T>
 *      type object
 */
public class Controller<T> {

    private File file;
    /**
     * 
     * 
     * @param f
     *          name of file
     */
    public void setFile(final String f) {
        this.file = new File(f);
    }
    /**
     * 
     * 
     * @return
     *  file
     */
    public File getFile() {
        return this.file;
    }
    /**
     * 
     * @return
     *        file path
     */
    public String getPathFile() {
        return this.file.getPath();
    }
    /**
     * 
     * @param m
     *          element to save in file
     * @throws IOException
     *          ex e
     */
    public void saveObject(final T m) throws IOException {
        try (OutputStream output = new FileOutputStream(this.file);
                ObjectOutputStream data = new ObjectOutputStream(output)) {
            if (m == null) {
                throw new IOException();
            } else {
                data.writeObject(m);
            }
        } catch (FileNotFoundException e1) {
                e1.printStackTrace();
            }
    }
    /*
     * This class must implement a simple controller responsible of I/O access. It
     * considers a single file at a time, and it is able to serialize objects in it.
     * 
     * Implement this class in such a way that:
     * 
     * 1) It has a method for setting a File as current file
     * 
     * 2) It has a method for getting the current File
     * 
     * 3) It has a method for getting the path (in form of String) of the current
     * File
     * 
     * 4) It has a method that gets a Serializable as input and saves such Object in
     * the current file. Remember how to use the ObjectOutputStream. This method may
     * throw IOException.
     * 
     * 5) By default, the current file is "output.dat" inside the user home folder.
     * A String representing the local user home folder can be accessed using
     * System.getProperty("user.home"). The separator symbol (/ on *nix, \ on
     * Windows) can be obtained as String through the method
     * System.getProperty("file.separator"). The combined use of those methods leads
     * to a software that run correctly on every platform.
     */

}
