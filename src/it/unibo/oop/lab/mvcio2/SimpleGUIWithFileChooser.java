package it.unibo.oop.lab.mvcio2;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import it.unibo.oop.lab.mvcio.SimpleGUI;
/**
 * A very simple program using a graphical interface.
 * 
 */
public final class SimpleGUIWithFileChooser extends SimpleGUI {
    /**
     * 
     * 
     */
    @SuppressWarnings("unused")
    public SimpleGUIWithFileChooser() {
        super();
        final JPanel panel = new JPanel(new BorderLayout());
        final JButton browse = new JButton("Browse..");
        final JTextField text = new JTextField();
        final JFileChooser choose = new JFileChooser();
        text.setEnabled(false);
        panel.add(text, BorderLayout.CENTER);
        panel.add(browse, BorderLayout.LINE_END);
        text.setText(this.getFile().getName());
        browse.addActionListener(e-> {
            if (JFileChooser.APPROVE_OPTION == choose.showSaveDialog(text)) {
                choose.getSelectedFile();
                this.setFile(choose.getSelectedFile().getPath());
                text.setText((choose.getSelectedFile().getName()));
            }
            if (JFileChooser.CANCEL_OPTION == 0) {
                JOptionPane.showMessageDialog(choose, "ERROR");
            }
        });
        this.getFrame().getContentPane().add(panel, BorderLayout.NORTH);
    }
    /**
     * 
     * 
     * @param strings
     *  lui
     */
    public static void main(final String...strings) {
        final SimpleGUIWithFileChooser s = new SimpleGUIWithFileChooser();
        s.getFrame().setVisible(true);
    }

    /*
     * TODO: Starting from the application in mvcio:
     * 
     * 1) Add a JTextField and a button "Browse..." on the upper part of the
     * graphical interface.
     * Suggestion: use a second JPanel with a second BorderLayout, put the panel
     * in the North of the main panel, put the text field in the center of the
     * new panel and put the button in the line_end of the new panel.
     * 
     * 2) The JTextField should be non modifiable. And, should display the
     * current selected file.
     * 
     * 3) On press, the button should open a JFileChooser. The program should
     * use the method showSaveDialog() to display the file chooser, and if the
     * result is equal to JFileChooser.APPROVE_OPTION the program should set as
     * new file in the Controller the file chosen. If CANCEL_OPTION is returned,
     * then the program should do nothing. Otherwise, a message dialog should be
     * shown telling the user that an error has occurred (use
     * JOptionPane.showMessageDialog()).
     * 
     * 4) When in the controller a new File is set, also the graphical interface
     * must reflect such change. Suggestion: do not force the controller to
     * update the UI: in this example the UI knows when should be updated, so
     * try to keep things separated.
     */

}
