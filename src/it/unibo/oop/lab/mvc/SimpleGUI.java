package it.unibo.oop.lab.mvc;

import java.awt.BorderLayout;
import javax.swing.JTextField;


import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

/**
 * A very simple program using a graphical interface.
 * 
 */
public final class SimpleGUI implements Controller {
    private final JFrame frame = new JFrame();
    private final List<String> list;
    private String temp;
    /**
     * 
     * 
     * @return
     *  frame
     */
    public JFrame getFrame() {
        return this.frame;
    }
     /**
     * 
     * 
     * @param args
     *  non lo so
     */
    public static void main(final String[] args) {
        final SimpleGUI f = new SimpleGUI();
        f.getFrame().setVisible(true);
    }
    /*
     * Once the Controller is done, implement this class in such a way that:
     * 
     * 1) I has a main method that starts the graphical application
     * 
     * 2) In its constructor, sets up the whole view
     * 
     * 3) The graphical interface consists of a JTextField in the upper part of the frame, 
     * a JTextArea in the center and two buttons below it: "Print", and "Show history". 
     * SUGGESTION: Use a JPanel with BorderLayout
     * 
     * 4) By default, if the graphical interface is closed the program must exit
     * (call setDefaultCloseOperation)
     * 
     * 5) The behavior of the program is that, if "Print" is pressed, the
     * controller is asked to show the string contained in the text field on standard output. 
     * If "show history" is pressed instead, the GUI must show all the prints that
     * have been done to this moment in the text area.
     * 
     */

    /**
     * builds a new {@link SimpleGUI}.
     * @return 
     */
    public SimpleGUI() {

        /*
         * Make the frame half the resolution of the screen. This very method is
         * enough for a single screen setup. In case of multiple monitors, the
         * primary is selected.
         * 
         * In order to deal coherently with multimonitor setups, other
         * facilities exist (see the Java documentation about this issue). It is
         * MUCH better than manually specify the size of a window in pixel: it
         * takes into account the current resolution.
         */
        list = new LinkedList<String>();
        final Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        final int sw = (int) screen.getWidth();
        final int sh = (int) screen.getHeight();
        frame.setSize(sw / 2, sh / 2);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        final JPanel panel = new JPanel(new BorderLayout());
        frame.add(panel);
        final JTextField view = new JTextField();
        final JTextArea area = new JTextArea();
        panel.add(view, BorderLayout.NORTH);
        panel.add(area, BorderLayout.CENTER);
        final JPanel subpanel = new JPanel(new FlowLayout());
        final JButton print = new JButton("Print");
        final JButton show = new JButton("Show history");
        subpanel.add(print);
        area.setLineWrap(true);
        subpanel.add(show);
        panel.add(subpanel, BorderLayout.SOUTH);
        print.addActionListener(e-> {
            final String s = view.getText();
            try {
                this.setString(s);
            } catch (NullException e1) {
                e1.printStackTrace();
            }
            printString();
        });
        show.addActionListener(e-> {
            String q = list.get(0) + "\n";
            int c = 0;
            for (final String d : list) {
                if (c == 0) {
                    c++;
                } else {
                    q = q + d + "\n";
                }
            }
            area.setText(q);
        });
        /*
         * Instead of appearing at (0,0), upper left corner of the screen, this
         * flag makes the OS window manager take care of the default positioning
         * on screen. Results may vary, but it is generally the best choice.
         */
        frame.setLocationByPlatform(true);
    }
    /**
     * 
     * @param s
     *  stringa
     * 
     * @throws NullException
     *  ex
     * 
     */
    public void setString(final String s) throws NullException {
        list.add(s);
        temp = s;
    }
    /**
     * 
     * @return stringa
     * 
     * 
     */
    public String getString() {
        return  temp;
    }
    /**
     * @return lista stringhe
     * 
     * 
     */
    public List<String> getList() {
        return list;
    }
    /**
     * 
     * @throws IllegalStateException
     *  ex
     * 
     */
    public void printString() throws IllegalStateException {
        System.out.println(getString());
    }

}