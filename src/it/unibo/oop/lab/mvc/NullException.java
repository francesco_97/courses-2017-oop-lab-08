package it.unibo.oop.lab.mvc;
/**
 * 
 * 
 * 
 *
 */
public class NullException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    NullException() {
      super("Attenzione, stai riferendo una stringa non inizializzata");
  }
}
